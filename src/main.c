#include <pebble.h>

static Window *window;
static TextLayer *text_layer;

#define STORAGE_KEY 99
#define VIBE_BT 1
#define VIBE_MIN 2

typedef struct persist {
    bool vibe_bt;
    bool vibe_min;
} __attribute__((__packed__)) persist;

persist settings = {
    .vibe_bt = true,
    .vibe_min = false
};


static void tick_handler(struct tm *tick_time, TimeUnits units_changed) {
    static char s_time_buffer[16];
    if (clock_is_24h_style()) {
        strftime(s_time_buffer, sizeof(s_time_buffer), "%H:%M", tick_time);
    } else {
        strftime(s_time_buffer, sizeof(s_time_buffer), "%I:%M", tick_time);
    };
    text_layer_set_text(text_layer, s_time_buffer);

    if ((units_changed & MINUTE_UNIT) && (settings.vibe_min)) {
        vibes_double_pulse();
    };
}

void bt_handler(bool connected) {
    if (settings.vibe_bt) {
        vibes_long_pulse();
    };
}

void in_received_handler(DictionaryIterator *received, void *context) {
    Tuple *vibe_bt_tuple = dict_find(received, VIBE_BT);
    Tuple *vibe_min_tuple = dict_find(received, VIBE_MIN);

    settings.vibe_bt = (bool)vibe_bt_tuple->value->int16;
    settings.vibe_min = (bool)vibe_min_tuple->value->int16;
}

static void window_load(Window *window) {
  Layer *window_layer = window_get_root_layer(window);
  GRect bounds = layer_get_bounds(window_layer);

  text_layer = text_layer_create((GRect) { .origin = { 0, 52 },
                                 .size = { bounds.size.w, 30 } });
  text_layer_set_text_alignment(text_layer, GTextAlignmentCenter);
  text_layer_set_font(text_layer, fonts_get_system_font(FONT_KEY_GOTHIC_28));
  layer_add_child(window_layer, text_layer_get_layer(text_layer));
}

static void window_unload(Window *window) {
  text_layer_destroy(text_layer);
}

static void init(void) {
    app_message_register_inbox_received(in_received_handler);

    const uint32_t inbound_size = 128;
    const uint32_t outbound_size = 128;

    app_message_open(inbound_size, outbound_size);

    if (persist_exists(STORAGE_KEY)) {
        persist_read_data(STORAGE_KEY, &settings, sizeof(settings));
    } else {
        persist_write_data(STORAGE_KEY, &settings, sizeof(settings));
    };

  window = window_create();
  window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,
    .unload = window_unload,
  });
  const bool animated = true;
  window_stack_push(window, animated);
  tick_timer_service_subscribe(MINUTE_UNIT, tick_handler);
  bluetooth_connection_service_subscribe(bt_handler);
}

static void deinit(void) {
  window_destroy(window);

  persist_write_data(STORAGE_KEY, &settings, sizeof(settings));
  tick_timer_service_unsubscribe();
  bluetooth_connection_service_unsubscribe();
}

int main(void) {
  init();

  APP_LOG(APP_LOG_LEVEL_DEBUG, "Done initializing, pushed window: %p", window);

  app_event_loop();
  deinit();
}
